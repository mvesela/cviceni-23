## 6.2.1-3 — 4.12.2020
### Activities
- Added [co-chteli-cerni-panteri](https://cviceni-test.historylab.now.sh/dev/co-chteli-cerni-panteri)
- Added [co-prinesla-nova-univerzita](https://cviceni-test.historylab.now.sh/dev/co-prinesla-nova-univerzita)
- Added [co-prinesla-zed](https://cviceni-test.historylab.now.sh/dev/co-prinesla-zed)
- Added [co-se-stalo-v-little-rock](https://cviceni-test.historylab.now.sh/dev/co-se-stalo-v-little-rock)
- Added [co-se-stalo-v-zime-1970](https://cviceni-test.historylab.now.sh/dev/co-se-stalo-v-zime-1970)
- Added [co-se-stalo-ve-volarech](https://cviceni-test.historylab.now.sh/dev/co-se-stalo-ve-volarech)
- Added [jak-informovali-o-havarii](https://cviceni-test.historylab.now.sh/dev/jak-informovali-o-havarii)
- Added [jak-prvni-svetova-valka-zmenila-hranice](https://cviceni-test.historylab.now.sh/dev/jak-prvni-svetova-valka-zmenila-hranice)
- Added [jak-vyobrazili-sve-vitezstvi-u-omdurmanu](https://cviceni-test.historylab.now.sh/dev/jak-vyobrazili-sve-vitezstvi-u-omdurmanu)
- Added [jaky-byl-jejich-navrat](https://cviceni-test.historylab.now.sh/dev/jaky-byl-jejich-navrat)
- Added [jaky-pribeh-vypraveji-kufry](https://cviceni-test.historylab.now.sh/dev/jaky-pribeh-vypraveji-kufry)
- Added [je-slavonin-mesto-nebo-venkov](https://cviceni-test.historylab.now.sh/dev/je-slavonin-mesto-nebo-venkov)
- Added [je-zlicin-mesto-nebo-venkov](https://cviceni-test.historylab.now.sh/dev/je-zlicin-mesto-nebo-venkov)
- Added [za-co-protestovali](https://cviceni-test.historylab.now.sh/dev/za-co-protestovali)
- Added activities for exhibition _UPM_
    - Added [co-si-slibovali-od-siciho-stroje](https://cviceni-test.historylab.now.sh/dev/co-si-slibovali-od-siciho-stroje)
    - Added [co-umoznil-fonograf](https://cviceni-test.historylab.now.sh/dev/co-umoznil-fonograf)
    - Added [jakou-zenu-vybrali-pro-reklamu](https://cviceni-test.historylab.now.sh/dev/jakou-zenu-vybrali-pro-reklamu)
    - Added [k-cemu-slouzi-siti](https://cviceni-test.historylab.now.sh/dev/k-cemu-slouzi-siti)
- Fixed [kam-ho-umistit](https://cviceni-test.historylab.now.sh/dev/kam-ho-umistit)
- Fixed [proc-fotili-vlajku-znovu](https://cviceni-test.historylab.now.sh/dev/proc-fotili-vlajku-znovu)
- Optimised [napalm-girl](https://cviceni-test.historylab.now.sh/dev/napalm-girl) `v3`
- Optimised [proc-jezdit-bezpecne](https://cviceni-test.historylab.now.sh/dev/proc-jezdit-bezpecne) `v3`
- Updated [proc-upravili-reportaz](https://cviceni-test.historylab.now.sh/dev/proc-upravili-reportaz)

### Tools
- Audio
    - Added transcript feature to simple audio player
- Fullscreen
    - Fixed deformation of image
- Selectable
    - Added option to have a gallery
- SVG
    - Objects cannot be dragged out of image

### Development
- Updated Node.js version to current LTS `v14`


## 6.2.0 — 30.9.2020
### Activities
- Fixed wrong or missing help in several activities

#### Public
- Added [proc-fotili-vlajku-znovu](https://cviceni-test.historylab.now.sh/dev/proc-fotili-vlajku-znovu)
- Optimised [co-pripomina-pomnik](https://cviceni-test.historylab.now.sh/dev/co-pripomina-pomnik)
- Optimised [co-si-mohli-myslet-lide-v-bufetu](https://cviceni-test.historylab.now.sh/dev/co-si-mohli-myslet-lide-v-bufetu)
- Optimised [jak-vnimali-boxersky-zapas](https://cviceni-test.historylab.now.sh/dev/jak-vnimali-boxersky-zapas)
- Optimised [kdo-byl-jejich-nepritel](https://cviceni-test.historylab.now.sh/dev/kdo-byl-jejich-nepritel)
- Optimised [kam-zmizeli-cesti-nemci](https://cviceni-test.historylab.now.sh/dev/kam-zmizeli-cesti-nemci)
- Optimised [kam-ho-umistit](https://cviceni-test.historylab.now.sh/dev/kam-ho-umistit)
- Optimised [mnichov-1938](https://cviceni-test.historylab.now.sh/dev/mnichov-1938)
- Optimised [proc-odcinit-bilou-horu](https://cviceni-test.historylab.now.sh/dev/proc-odcinit-bilou-horu)
- Optimised [proc-prejmenovali-ulice](https://cviceni-test.historylab.now.sh/dev/proc-prejmenovali-ulice)
- Optimised [proc-vydavali-muj-boj](https://cviceni-test.historylab.now.sh/dev/proc-vydavali-muj-boj)
- Optimised [proc-zaci-psali-rezoluci](https://cviceni-test.historylab.now.sh/dev/proc-zaci-psali-rezoluci)
- Optimised [proc-zasedali](https://cviceni-test.historylab.now.sh/dev/proc-zasedali)
- Optimised [videt-a-slyset-srpnovou-okupaci](https://cviceni-test.historylab.now.sh/dev/videt-a-slyset-srpnovou-okupaci)
- Improved [proc-ocistovali-kravy](https://cviceni-test.historylab.now.sh/dev/proc-ocistovali-kravy)

#### Test
- Added activities for schoolbook test
    - Added [co-kluci-provedli](https://cviceni-test.historylab.now.sh/dev/co-kluci-provedli)
    - Added [co-se-dozvime-z-propagandistickych-plakatu](https://cviceni-test.historylab.now.sh/dev/co-se-dozvime-z-propagandistickych-plakatu)
    - Added [co-umeni-vypovida-o-valce](https://cviceni-test.historylab.now.sh/dev/co-umeni-vypovida-o-valce)
    - Added [jak-zobrazila-hospodarskou-krizi](https://cviceni-test.historylab.now.sh/dev/jak-zobrazila-hospodarskou-krizi)
    - Added [meli-ukrajinci-odstranit-lenina](https://cviceni-test.historylab.now.sh/dev/meli-ukrajinci-odstranit-lenina)
    - Added [meli-udat-cyklistu](https://cviceni-test.historylab.now.sh/dev/meli-udat-cyklistu)
    - Added [proc-nevyhrali-valku](https://cviceni-test.historylab.now.sh/dev/proc-nevyhrali-valku)
    - Added [vojak-na-fotografii](https://cviceni-test.historylab.now.sh/dev/vojak-na-fotografii)
- Added activities for exhibition _Repor Tvář Julia Fučíka_
    - Added [komu-psal-fucik-rtjf](https://cviceni-test.historylab.now.sh/dev/komu-psal-fucik-rtjf)
    - Added [meli-ho-odstranit-rtjf](https://cviceni-test.historylab.now.sh/dev/meli-ho-odstranit-rtjf)
    - Added [proc-se-shromazdili-rtfj](https://cviceni-test.historylab.now.sh/dev/proc-se-shromazdili-rtfj)
    - Added [proc-upravili-reportaz-rtjf](https://cviceni-test.historylab.now.sh/dev/proc-upravili-reportaz-rtjf)

### General
- **Unified naming of files and ids**
    - renamed files which does not meet naming convention requirements
    - renamed `id` of tools which does not naming convention requirements
    - updated `doc/jak-vytvorit-nove-cviceni.md` with the naming convention requirements
- **Exhibition mode**
    - Added URL parameter `exid` (modifies the entry form; saved to data as `user.form.exhibition`)
    - Added URL parameter `redirect` (used by backend to redirect on confirmation page; saved to data as `user.form.redirect`)

### Tools
- Fullscreen
    - Added option to have an alternative image for fullscreen (gallery module supported at this moment)
- Gallery
    - **Added option to show selection from Selectable**
    - Added caption on hover also for text items
- Keywords
    - Fixed control buttons in wordcloud
    - Fixed positioning with submodule
- Magnifying Glass
    - Fixed when the glass is out of window screen
- Media
    - Added support for image
    - Removed `max-width` from media items & enlarge its width
- Selectable
    - **Added support for dynamic data for gallery and user text questions**
    - Improved design and interaction
- Sortable
    - Fixed bugs
- Sources
    - Added multirow grid layout when more items than 4
- SVG
    - Added option to change default color of path (drawing) via JSON
- Text editor
    - Improved touch support
- User text
    - **Added option to show question based on selection in Selectable module**

### Development
- Added script to easily change name of an activity (and all its folders, files & data)
- Added [gulp.lastRun()](https://gulpjs.com/docs/en/api/lastrun) on some tasks
- Updated data scaffolding template with new metadata properties
- More generous support of older browsers (`browserslist`)


## 6.1.1 — 18.6.2020
### Activities
- Added [kam-ho-umistit](https://cviceni-test.historylab.now.sh/dev/kam-ho-umistit)
- Added [proc-vydavali-muj-boj](https://cviceni-test.historylab.now.sh/dev/proc-vydavali-muj-boj)
- Added PDF [co-si-mohli-myslet-lide-v-bufetu](https://cviceni-test.historylab.now.sh/dev/co-si-mohli-myslet-lide-v-bufetu)
- Updated PDF [jak-obhajit-pisen](https://cviceni-test.historylab.now.sh/dev/jak-obhajit-pisen)

### General
- Added GDPR notice
- Improved legibility of captions
- Improved english translations

### Tools
- Keywords
    - Improved resize transition
- SVG
    - Added orange color
- Text editor
    - Added typewritter font style

### Development
- Generate thumbnails of all images for use outside of activity (with `-thumb` sufix)


## 6.1.0 — 1.6.2020
### Activities
- Added [co-pripomina-pomnik](https://cviceni-test.historylab.now.sh/dev/co-pripomina-pomnik)
- Added [co-si-mohli-myslet-lide-v-bufetu](https://cviceni-test.historylab.now.sh/dev/co-si-mohli-myslet-lide-v-bufetu)
- Added [jak-vnimali-boxersky-zapas](https://cviceni-test.historylab.now.sh/dev/jak-vnimali-boxersky-zapas)
- Added [proc-prejmenovali-ulice](https://cviceni-test.historylab.now.sh/dev/proc-prejmenovali-ulice)
- Added [proc-zasedali](https://cviceni-test.historylab.now.sh/dev/proc-zasedali)
- Optimised [jake-narodni-symboly-potrebujeme](https://cviceni-test.historylab.now.sh/dev/jake-narodni-symboly-potrebujeme)
- Optimised & rename [jeden-znak-dva-vyznamy](https://cviceni-test.historylab.now.sh/dev/jeden-znak-dva-vyznamy) (former `jak-se-menil-vyznam-17-listopadu`)
- Optimised [k-cemu-jsou-nam-vyroci](https://cviceni-test.historylab.now.sh/dev/k-cemu-jsou-nam-vyroci)
- Optimised [k-cemu-vyzyvaly-dva-tisice-slov-a-charta-77](https://cviceni-test.historylab.now.sh/dev/k-cemu-vyzyvaly-dva-tisice-slov-a-charta-77)
- Optimised [proc-demonstrovali](https://cviceni-test.historylab.now.sh/dev/proc-demonstrovali)
- Optimised [proc-jezdit-bezpecne](https://cviceni-test.historylab.now.sh/dev/proc-jezdit-bezpecne)
- Optimised [proc-vytvareli-mapu](https://cviceni-test.historylab.now.sh/dev/proc-vytvareli-mapu)
- Optimised [s-trabanty-za-svobodou](https://cviceni-test.historylab.now.sh/dev/s-trabanty-za-svobodou)

### General
- Tooltips refactored to use `data-tooltip` value via CSS
- Fixed issue with wrong URL to images on server (replace `cviceni-aktualni` with `cviceni-verze/vX-Y`)

### Tools
- Audio
    - Added option to add textual transcript which is shown within fullscreen module
- Draggable
    - Added fullscreen feature for images and SVG
- Gallery
    - Added fullscreen feature for SVG
- Keywords
    - Added option for positive selection
- Sortable
    - Hide captions on hover
    - Hide captions on previous halfslide
- SVG
    - Added fullscreen feature
    - Added option to forbid adding text and changing color
    - Added option to have custom labels for cpredefined comic bubbles
- Table
    - Added support for duplication of tags from drop cell of different table
    - Added support for multiple tables in the same way as it is in other modules
    - Tweak CSS for multiple tables & table with draggable submodule

### Development
- Updated [release checklist](https://bitbucket.org/historylab/cviceni/src/master/doc/release-checklist.md) with information how to make hotfix of an old version


## 6.0.2 — 20.5.2020
### General
- Added option to anonymize emails within saved activity with URL parameter `anon=1`


## 6.0.1 — 24.4.2020
### Activities
- Fix PDF [co-prinese-mir](https://cviceni-test.historylab.now.sh/dev/co-prinese-mir)
- Fix PDF [proc-vydali-znamku](https://cviceni-test.historylab.now.sh/dev/proc-vydali-znamku)

### Tools
- Fixed missing horizontal border in a specific case of new table


## 6.0.0 — 23.4.2020
### Activities
- Optimised [co-se-stalo-v-abertamech](https://cviceni-test.historylab.now.sh/dev/co-se-stalo-v-abertamech)
- Optimised [co-prinese-mir](https://cviceni-test.historylab.now.sh/dev/co-prinese-mir)
- Optimised [co-tvrdil-atlas](https://cviceni-test.historylab.now.sh/dev/co-tvrdil-atlas)
- Optimised [jak-mely-deti-travit-volny-cas](https://cviceni-test.historylab.now.sh/dev/jak-mely-deti-travit-volny-cas)
- Optimised [jak-obhajit-pisen](https://cviceni-test.historylab.now.sh/dev/jak-obhajit-pisen)
- Optimised [jak-si-pripominame-listopad-na-albertove](https://cviceni-test.historylab.now.sh/dev/jak-si-pripominame-listopad-na-albertove)
- Optimised [mel-si-kleknout](https://cviceni-test.historylab.now.sh/dev/mel-si-kleknout)
- Optimised [mnichov-1938](https://cviceni-test.historylab.now.sh/dev/mnichov-1938)
- Optimised [politika-na-namesti](https://cviceni-test.historylab.now.sh/dev/politika-na-namesti)
- Optimised [prezident-masaryk-na-fotografii](https://cviceni-test.historylab.now.sh/dev/prezident-masaryk-na-fotografii)
- Optimised [proc-je-velka-britanie-zemi-caj](https://cviceni-test.historylab.now.sh/dev/proc-je-velka-britanie-zemi-caj)
- Optimised [proc-resit-zidovskou-otazku](https://cviceni-test.historylab.now.sh/dev/proc-resit-zidovskou-otazku)
- Optimised [proc-vydali-znamku](https://cviceni-test.historylab.now.sh/dev/proc-vydali-znamku)
- Optimised [promeny-mesta-mostu](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-mostu)
- Optimised & renamed [rozruch-na-namesti](https://cviceni-test.historylab.now.sh/dev/rozruch-na-namesti) (Co přinesl nástup rozhlasu?)
- Optimised [how-did-they-celebrate-a-public-holiday](https://cviceni-test.historylab.now.sh/dev/how-did-they-celebrate-a-public-holiday)
- Optimised [the-village-of-horni-vysokes-transformation](https://cviceni-test.historylab.now.sh/dev/the-village-of-horni-vysokes-transformation)
- Optimised [what-happened-at-croydon-airport](https://cviceni-test.historylab.now.sh/dev/what-happened-at-croydon-airport)
- Optimised [why-were-they-displaced](https://cviceni-test.historylab.now.sh/dev/why-were-they-displaced)

### General
- "Exercises" are now "activities" in english

### Tools
- Added, improved & fixed layouts when module + submodule
    - keywords with gallery
    - keywords with table
    - table with draggable
    - table with gallery
    - test quiz with gallery
    - userText with table
- Improved responsivity
- Draggable
    - Added option to turn off indication that draggable item was dropped
    - Remove draggable as standalone module, is used as submodule only
- Sortable
    - **Improved feedback logic**
    - Fixed dragging from within `overflow: hidden` elements (dropping is still bugged in Firefox tho)
- SVG
    - Added caption feature
    - Added option to have predefined circles
    - Fixed sizing
- Table
    - **Improved design of the new universal table** & fixed many bugs
    - **Removed analytical table, generic table, editorPlaceholderNaTagy** replaced with the new universal table
    - table usable as submodule to show dynamic data as a text

### Development
- **Removed custom js store & observer for communication between modules (use standard Event interface now)**
- Minor versions are also historical releases now (means they are stored and used to load saved activity)
- Separate branches for production (`master`) and test (`staging`) server


## 5.12 — 17.3.2020
### Exercises
- Optimised [co-ministr-vzkazuje-ucitelum](https://cviceni-test.historylab.now.sh/dev/co-ministr-vzkazuje-ucitelum)
- Optimised [co-se-stalo-na-letisti-croydon](https://cviceni-test.historylab.now.sh/dev/co-se-stalo-na-letisti-croydon)
- Optimised [co-se-ucime-krome-pocitani](https://cviceni-test.historylab.now.sh/dev/co-se-ucime-krome-pocitani)
- Optimised [jak-oslavovali-statni-svatek](https://cviceni-test.historylab.now.sh/dev/jak-oslavovali-statni-svatek)
- Optimised [jak-psat-o-uprchlicich](https://cviceni-test.historylab.now.sh/dev/jak-psat-o-uprchlicich)
    - removed custom controller
- Optimised [proc-vystavovali-obraz-t-g-masaryka](https://cviceni-test.historylab.now.sh/dev/proc-vystavovali-obraz-t-g-masaryka)
- Optimised [proc-vznikl-skolni-obraz](https://cviceni-test.historylab.now.sh/dev/proc-vznikl-skolni-obraz)

### General
- Added autocomplete to user inputs on the first slide

### Development
- Updated Node.js version to current LTS v12
- Updated `imagemin` to 7
- Updated `rollup` to 2


## 5.11.4 — 10.2.2020
### Exercises
- Fixed help in [netradicni-mezniky-ceskoslovenskych-dejin](https://cviceni-test.historylab.now.sh/dev/netradicni-mezniky-ceskoslovenskych-dejin)
- Fixed ids in [promeny-mesta-zlina](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-zlina)


## 5.11.3 — 7.2.2020
### Exercises
- Fixed instruction & typo [co-nataceli](https://cviceni-test.historylab.now.sh/dev/co-nataceli)


## 5.11.2 — 7.2.2020
### Exercises
- Fixed missing help [promeny-mesta-zlina](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-zlina)


## 5.11.1 — 7.2.2020
### Exercises
- Fixed keywords, anotation & time [co-delaji-zeny-na-fotografii](https://cviceni-test.historylab.now.sh/dev/co-delaji-zeny-na-fotografii)


## 5.11 — 7.2.2020
### Exercises
- Added [kdo-byl-jejich-nepritel](https://cviceni-test.historylab.now.sh/dev/kdo-byl-jejich-nepritel)
- Optimised [co-nataceli](https://cviceni-test.historylab.now.sh/dev/co-nataceli)
- Optimised [netradicni-mezniky-ceskoslovenskych-dejin](https://cviceni-test.historylab.now.sh/dev/netradicni-mezniky-ceskoslovenskych-dejin)
- Optimised & renamed [proc-ocistovali-kravy](https://cviceni-test.historylab.now.sh/dev/proc-ocistovali-kravy)
- Optimised [proc-sedeli-ve-skole](https://cviceni-test.historylab.now.sh/dev/proc-sedeli-ve-skole)
- Optimised & fixed the slug [promeny-mesta-zlina](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-zlina)
- Optimised [to-byla-slava](https://cviceni-test.historylab.now.sh/dev/to-byla-slava)

### Tools
- **Added support to trasfer an order from Sortable tool to other multiitem tools**
    - Only SVG is supported now
    - See [this commit](https://bitbucket.org/historylab/cviceni/commits/dc24770f471ef2e5b5bce8f2088ba199396bdd2e)

### Development
- Updated [jak-vytvorit-nove-cviceni](https://bitbucket.org/historylab/cviceni/src/dev/doc/jak-vytvorit-nove-cviceni.md) with information about image processing


## 5.10.8 — 18.12.2019
### Exercises
- Added testing version of [videt-a-slyset-srpnovou-okupaci-2](https://cviceni-test.historylab.now.sh/dev/videt-a-slyset-srpnovou-okupaci-2)


## 5.10.7 — 10.12.2019
### Exercises
- Added new image to [co-prinesl-nastup-rozhlasu](https://cviceni-test.historylab.now.sh/dev/co-prinesl-nastup-rozhlasu/)
- Improved quality of images in [jake-narodni-symboly-potrebujeme](https://cviceni-test.historylab.now.sh/dev/jake-narodni-symboly-potrebujeme)
- Improved quality of images in [proc-je-velka-britanie-zemi-caje](https://cviceni-test.historylab.now.sh/dev/proc-je-velka-britanie-zemi-caje)


## 5.10.6 — 5.12.2019
- Add missing PDFs for:
    - `co-prinese-mir`
    - `jak-se-menil-vyznam-17-listopadu-3`
    - `proc-je-velka-britanie-zemi-caje`
    - `proc-vydali-znamku`


## 5.10.5 — 4.12.2019
- Reverted previous version


## 5.10.4 — 4.12.2019
- Revert IDs for SVG tools to not break up user data in [proc-je-velka-britanie-zemi-caje](https://cviceni-test.historylab.now.sh/dev/proc-je-velka-britanie-zemi-caje)


## 5.10.3 — 4.12.2019
### Exercises
- Added [jaky-pribeh-vypravi-tank](https://cviceni-test.historylab.now.sh/dev/jaky-pribeh-vypravi-tank)
- Updated [co-prinese-mir](https://cviceni-test.historylab.now.sh/dev/co-prinese-mir)
- Updated [co-se-stalo-v-abertamech](https://cviceni-test.historylab.now.sh/dev/co-se-stalo-v-abertamech)
- Updated [jak-se-menil-vyznam-17-listopadu-3](https://cviceni-test.historylab.now.sh/dev/jak-se-menil-vyznam-17-listopadu-3)
- Updated [jak-si-pripominame-listopad-na-albertove-3](https://cviceni-test.historylab.now.sh/dev/jak-si-pripominame-listopad-na-albertove-3)
- Updated [jak-videli-zenu](https://cviceni-test.historylab.now.sh/dev/jak-videli-zenu)
- Updated [politika-na-namesti](https://cviceni-test.historylab.now.sh/dev/politika-na-namesti)
- Updated [proc-vydali-znamku](https://cviceni-test.historylab.now.sh/dev/proc-vydali-znamku)
- Updated [proc-vytvareli-mapu](https://cviceni-test.historylab.now.sh/dev/proc-vytvareli-mapu)
- Updated [trabanty-za-svobodu](https://cviceni-test.historylab.now.sh/dev/trabanty-za-svobodu) (right name is „S trabanty za svobodou“)


## 5.10.2 — 30.11.2019
### Exercises
- [proc-je-velka-britanie-zemi-caje](https://cviceni-test.historylab.now.sh/dev/proc-je-velka-britanie-zemi-caje): Removed SVG from draggable, use plain images
- Fixed missing `pretahovani` to global `cviceni.funkce`
    - affected exercies `co-prinese-mir`, `proc-vydali-znamku`, `jak-si-pripominame-listopad-na-albertove-3`
    
### General
- Add error message when element to be loaded with data does not exists and break if problem to continue loading loop

### Tools
- Save SVG even when in draggable to fix problem with loading data


## 5.10.1 — 29.11.2019
### Exercises
- Added [co-prinese-mir](https://cviceni-test.historylab.now.sh/dev/co-prinese-mir)
- Added [jak-se-menil-vyznam-17-listopadu-3](https://cviceni-test.historylab.now.sh/dev/jak-se-menil-vyznam-17-listopadu-3)
- Added [jake-narodni-symboly-potrebujeme](https://cviceni-test.historylab.now.sh/dev/jake-narodni-symboly-potrebujeme)
- Added [proc-je-velka-britanie-zemi-caje](https://cviceni-test.historylab.now.sh/dev/proc-je-velka-britanie-zemi-caje)
- Added [proc-vydali-znamku](https://cviceni-test.historylab.now.sh/dev/proc-vydali-znamku)

- Added PDF guides

### Tools
- SVG
    - Fixed (temporary) Firefox bug when dropping objects into SVG when on right side of splitscreen
- Universal table
    - Fixed selected nodes not to be removed when removed in text editor


## 5.10 — 29.11.2019
### General
- Update partner's logos

### Exercises
- **Added new user text field to the end of every exercise**

- Added [co-tvrdil-atlas](https://cviceni-test.historylab.now.sh/dev/co-tvrdil-atlas)
- Added [jak-obhajit-pisen](https://cviceni-test.historylab.now.sh/dev/jak-obhajit-pisen)
- Added [jak-si-pripominame-listopad-na-albertove-3](https://cviceni-test.historylab.now.sh/dev/jak-si-pripominame-listopad-na-albertove-3)
- Added [mel-si-kleknout](https://cviceni-test.historylab.now.sh/dev/mel-si-kleknout)
- Added [jak-mely-deti-travit-volny-cas](https://cviceni-test.historylab.now.sh/dev/jak-mely-deti-travit-volny-cas)
- Added [jak-videli-zenu](https://cviceni-test.historylab.now.sh/dev/jak-videli-zenu)
- Added [politika-na-namesti](https://cviceni-test.historylab.now.sh/dev/politika-na-namesti)
- Added [proc-demonstrovali](https://cviceni-test.historylab.now.sh/dev/proc-demonstrovali)
- Added [proc-upravili-reportaz](https://cviceni-test.historylab.now.sh/dev/proc-upravili-reportaz)
- Optimised [k-cemu-jsou-nam-vyroci](https://cviceni-test.historylab.now.sh/dev/k-cemu-jsou-nam-vyroci) `v3`
- Optimised [co-prinesl-nastup-rozhlasu](https://cviceni-test.historylab.now.sh/dev/co-prinesl-nastup-rozhlasu) `v2`
- Optimised [trabanty-za-svobodu](https://cviceni-test.historylab.now.sh/dev/trabanty-za-svobodu) `v2`
- Fixed [vanocni-zmena](https://cviceni-test.historylab.now.sh/dev/vanocni-zmena)
- Updated [proc-vytvareli-mapu](https://cviceni-test.historylab.now.sh/dev/proc-vytvareli-mapu)
- Updated & renamed [co-se-stalo-v-abertamech](https://cviceni-test.historylab.now.sh/dev/co-se-stalo-v-abertamech) (promeny-obce-abertamy)

### Tools
- Improved responsivity
    - Gallery
    - SVG
    - Text editor
    - Universal table

- **Added new Draggable tool**
    - used as submodul
    - supported items: keywords, images, svgs, audio, video
    - supported tools: SVG, universal table
    - layouts: horizontal & vertical
- SVG
    - **Added support for drop events from the new draggable tool**
        - use `"drop": true`
    - Improved visual design
    - Improved interaction with overlaped texts when exercise has been done
    - Fixed chained duplication
    - Fixed `max-width` calculation
- Text editor
    - Improve styling & interaction
- Universal table
    - **Added support for drop events from the new draggable tool**
        - use `"type": {"name": "drop"}` either for `rows` or `columns`
    - Improved logic for creating cells
    - Fixed responsive behaviour

### Development
- JavaScript
    - Destructuring allowed
- SVG
    - Switched practical effect of vertical & horizontal layout because of submodules (WIP, markup & styling needs refactor)
- Text editor
    - Refactor extracting selected nodes becasue of change of styling


## 5.9.9 — 13.11.2019
### Exercises
- Added [trabanty-za-svobodu](https://cviceni-test.historylab.now.sh/dev/trabanty-za-svobodu)
- Updated [k-cemu-jsou-nam-vyroci](https://cviceni-test.historylab.now.sh/dev/k-cemu-jsou-nam-vyroci) `v2`
    - with duplicated SVGs in `sortable` instead of simple images

### Tools
- Fixed missing condition check for every tool whether exercise has been completed or not to prevent unnecessary and possibly conflicting initializing
- Fixed rendering of `sortable` in horizontal layout (eg. with SVG in Chrome)
- Fixed `textarea` in SVG being interactive when being `sortable`

### Development
- Added `gulp data` task & updated `npm run catalogs` with it
- `sortable` does not need manually set item's `id`


## 5.9.8 — 10.11.2019
### Exercises
- Fixed image name in new variant of [jak-se-menil-vyznam-17-listopadu](https://cviceni-test.historylab.now.sh/dev/jak-se-menil-vyznam-17-listopadu) (test purpose)

### Catalogs
- Add [co-se-deje-na-fotografii](https://cviceni-test.historylab.now.sh/dev/co-se-deje-na-fotografii) to `test`


## 5.9.7 — 8.11.2019
### Exercises
- Fixed image names in new variant of [jak-se-menil-vyznam-17-listopadu](https://cviceni-test.historylab.now.sh/dev/jak-se-menil-vyznam-17-listopadu) (test purpose)


## 5.9.6 — 8.11.2019
### Exercises
- Added new variant of [jak-se-menil-vyznam-17-listopadu](https://cviceni-test.historylab.now.sh/dev/jak-se-menil-vyznam-17-listopadu) (test purpose)
- Added new variant of [jak-si-pripominame-listopad-na-albertove](https://cviceni-test.historylab.now.sh/dev/jak-si-pripominame-listopad-na-albertove) (test purpose)
- Fix missing non-braking spaces in texts in [k-cemu-jsou-nam-vyroci](https://cviceni-test.historylab.now.sh/dev/k-cemu-jsou-nam-vyroci)

### Tools
- Fixed calculation of max width of SVG
- Fixed not centered text inside new table in completed exercise


## 5.9.5 — 1.11.2019
### Exercises
- Added [k-cemu-jsou-nam-vyroci](https://cviceni-test.historylab.now.sh/dev/k-cemu-jsou-nam-vyroci)

### Tools
- Fixed new, universal table
    - to save & load user data
    - make it full width


## 5.9.4 — 31.10.2019
### Exercises
- Add missing anotation [jak-se-menil-vyznam-17-listopadu](https://cviceni-test.historylab.now.sh/dev/jak-se-menil-vyznam-17-listopadu)
- Tweak instruction [jak-si-pripominame-listopad-na-albertove](https://cviceni-test.historylab.now.sh/dev/jak-si-pripominame-listopad-na-albertove)

### General
- Fixed navigation condition to ignore non-existent slide

### Tools
- Fixed issue with unique ID for duplicated SVGs in gallery/sortable/selectable which failed loading user data
    - affected exercises: `jak-si-pripominame-listopad-na-albertove` and `co-prinesl-nastup-rozhlasu`


## 5.9.3 — 30.10.2019
### Exercises
- Added updated PDF for [to-byla-slava](https://cviceni-test.historylab.now.sh/dev/to-byla-slava/) `v3`


## 5.9.2 — 30.10.2019
### Tools
- Fix new visual feedback for sortable & selectable


## 5.9.1 — 30.10.2019
### Tools
- Fix new visual feedback for sortable & selectable


## 5.9.0 — 30.10.2019
### Exercises
- Added [jak-se-menil-vyznam-17-listopadu](https://cviceni-test.historylab.now.sh/dev/jak-se-menil-vyznam-17-listopadu/)
- Fixed [promeny-mesta-zlin](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-zlin/)
- Optimised [jak-si-pripominame-listopad-na-albertove](https://cviceni-test.historylab.now.sh/dev/jak-si-pripominame-listopad-na-albertove/) `v2`
- Optimised [jakou-budoucnost-nabizeli](https://cviceni-test.historylab.now.sh/dev/jakou-budoucnost-nabizeli/) `v2`
- Optimised [to-byla-slava](https://cviceni-test.historylab.now.sh/dev/to-byla-slava/) `v3`

### General
- Fixed typesetting
    - case with bracket followed by single character

### Tools
- **Added support of captions within a sortable item**
- Addded support for predefined value in user textarea
- Improved image sizing in selectable
- Improved visual feedback in selectable & sortable
- Improved layout with media players


## 5.8.0 — 17.10.2019
### Exercises
- Added [co-prinesl-nastup-rozhlasu](https://cviceni-test.historylab.now.sh/dev/co-prinesl-nastup-rozhlasu/)
- Fixed [jakou-budoucnost-nabizeli](https://cviceni-test.historylab.now.sh/dev/jakou-budoucnost-nabizeli/)
- Optimised [proc-byli-uneseni](https://cviceni-test.historylab.now.sh/dev/proc-byli-uneseni/) `v3`
- Optimised [vanocni-zmena](https://cviceni-test.historylab.now.sh/dev/vanocni-zmena/) `v2`
- Updated PDF for `promeny-mesta-zlin`

### Tools
- **Added support of SVG in a gallery** [#98](https://bitbucket.org/historylab/cviceni/pull-requests/98/feature-gallery-svg-support)
    - to add a caption on hover is possible as it is for normal image
- Fixed loading data for non-interactive duplicated SVG. Affected components: sortable, gallery.


## 5.7.0 — 14.10.2019
### General
- **Dropped support of Internet Explorer 11** [#87](https://bitbucket.org/historylab/cviceni/pull-requests/87/fix-drop-support-ie11)
    - set clear rules to be consistent and to improve support for actually supported browsers (eg. Edge)

### Exercises
- Added [husitstvi-a-historici](https://cviceni-test.historylab.now.sh/dev/husitstvi-a-historici/)
- Added [lipany-v-ucebnicich](https://cviceni-test.historylab.now.sh/dev/lipany-v-ucebnicich/)
- Added [mladi-jana-husa](https://cviceni-test.historylab.now.sh/dev/mladi-jana-husa/)
- Added [obrazy-jana-husa](https://cviceni-test.historylab.now.sh/dev/obrazy-jana-husa/)
- Fixed wrong image in [kam-zmizeli-cesti-nemci](https://cviceni-test.historylab.now.sh/dev/kam-zmizeli-cesti-nemci/)
- Fixed wrong caption in [horakova-rezoluce](https://cviceni-test.historylab.now.sh/dev/horakova-rezoluce/)

### Tools
- **New universal table module** to show data from other modules [#90](https://bitbucket.org/historylab/cviceni/pull-requests/90/feature-new-table)
    - supported at the moment: duplictated text, editor tag, select, input\[type="text\], custom data object
    - analytical table and generic table is going to be replaced with this module
- **Sortable supports SVG** [#84](https://bitbucket.org/historylab/cviceni/pull-requests/84/feature-sortable-svg)
- Fixed outline around play button in video
- Fixed position of close button in writing editor

### Development
- **New script to generate catalogs** [#88](https://bitbucket.org/historylab/cviceni/pull-requests/88/add-script-to-generate-catalogues)
    - use new property in each exercise JSON `cviceni.katalog` (`test`, `public` as possible values at the moment)
- Added `gulp js` task
    

## 5.6.2 — 11.10.2019
### Development
- Improved `release-checklist.md`
    - Added note to not forget document changes in `CHANGELOG.md`
    - Added more specific guide how to merge to `master`

### Exercises
- Optimised [proc-byli-vysidleni](https://cviceni-test.historylab.now.sh/dev/proc-byli-vysidleni/) `v2`
- Optimised [proc-resit-zidovskou-otazku](https://cviceni-test.historylab.now.sh/dev/proc-resit-zidovskou-otazku/) `v2`

### Tools
- Fix position of close button in writing editor


## 5.6.1 — 10.10.2019
### Tools
- Fixed SVG on Firefox if on right side of splitscreen


## 5.6.0 — 26.9.2019
### Exercises
- Added [jakou-budoucnost-nabizeli](https://cviceni-test.historylab.now.sh/dev/jakou-budoucnost-nabizeli/)

### Development
- Upgrade to Gulp 4
- Remove static asset revisioning
- Move build/test/deploy scripts to `./scripts` folder

### Tools
- **New iconography** for all tools & other UI
- Enlarge premade comic bubbles in SVG


## 5.5.3 — 10.9.2019
### Exercises
- Fixed layout in [promeny-mesta-zlin](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-zlin/)

### Tools
- Fixed calculation of local postion in SVG for Firefox when within transformed container. Affected exercises:
    - [promeny-mesta-zlin](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-zlin/)
    - [vanocni-zmena](https://cviceni-test.historylab.now.sh/dev/vanocni-zmena/)


## 5.5.2 — 29.8.2019
### Exercises
- Fixed typo in [co-delaji-zeny-na-fotografii](https://cviceni-test.historylab.now.sh/dev/co-delaji-zeny-na-fotografii/)
- Update keywords & pdf for [napalm-girl](https://cviceni-test.historylab.now.sh/dev/napalm-girl/)


## 5.5.1 — 28.8.2019
### Exercises
- Fixed missing `pdf.doporucenyPostup`

### Development
- Add `pdf.doporucenyPostup` to scaffold data file


## 5.5.0 — 28.8.2019
### Exercises
- Optimised [`co-delaji-zeny-na-fotografii`](https://cviceni-test.historylab.now.sh/dev/co-delaji-zeny-na-fotografii/) `v2`
    - Added `svg.duplikace`
- Optimised [`deset-let-po-revoluci-dekujeme-odejdete`](https://cviceni-test.historylab.now.sh/dev/deset-let-po-revoluci-dekujeme-odejdete/) `v3`
- Optimised [`napalm-girl`](https://cviceni-test.historylab.now.sh/dev/napalm-girl/) `v3`
- Fixed [`proc-resit-zidovskou-otazku`](https://cviceni-test.historylab.now.sh/dev/proc-resit-zidovskou-otazku)
  - wrong image
  - typos
- Optimised [`proc-sedeli-ve-skole`](https://cviceni-test.historylab.now.sh/dev/proc-sedeli-ve-skole/) `v2`
    - Added `svg.duplikace`
- Optimised & fixed [`promeny-obce-horni-vysoke`](https://cviceni-test.historylab.now.sh/dev/promeny-obce-horni-vysoke/) `v3`
    - Fixed size of SVG item due to wrong image sizes
- Optimised [`promeny-obce-kyselka`](https://cviceni-test.historylab.now.sh/dev/promeny-obce-kyselka/) `v2`
- Optimised [`vztycovani-vlajky`](https://cviceni-test.historylab.now.sh/dev/vztycovani-vlajky/) `v3`

### Development
- **When creating new exercise with the command there is no need for `--slug` argument.** It is generated automatically if not present.
- Improved build time if `--cviceni` is used
- Renamed all `faksimile` names to `gallery`
- Removed `exibition` and `homepage` assets
- Added `variables/_colors.scss`

### General
- Preload fonts to prevent flash of invisible text

### Tools
- **Added new selectable tool** ([73](https://bitbucket.org/historylab/cviceni/pull-requests/73))
- **Added character counter to user text fields** ([71](https://bitbucket.org/historylab/cviceni/pull-requests/71))
    - shows actual length and maximum length, eg. `116/400`
- **Added duplicate feature between user text fields** ([74](https://bitbucket.org/historylab/cviceni/pull-requests/74))
    - new property `duplikovat` for `uzivatelskyText.otazky[otazka]` (Boolean)
- Improved visual style of SVG elements & radial menu ([72](https://bitbucket.org/historylab/cviceni/pull-requests/72))
- Fixed initializing of user text fields and SVGs when exercise has user data
- Fixed clickable area in audio player


## 5.4.2 — 27.7.2019
### Exercises
- Fixed missing translation in [`why-were-they-displaced`](https://cviceni-test.historylab.now.sh/dev/why-were-they-displaced/)
- Fixed pdf [`why-were-they-displaced`](https://cviceni-test.historylab.now.sh/dev/why-were-they-displaced/)
- Fixed caption in [`how-did-they-celebrate-a-public-holiday`](https://cviceni-test.historylab.now.sh/dev/how-did-they-celebrate-a-public-holiday/)

### Tools
- Added back SVG comics bubble placeholder & improved its label legibility


## 5.4.1 — 26.7.2019
### General
- Fixed `typeset()` error if no text

## 5.4.0 — 26.7.2019
### Exercises
- Added [`how-did-they-celebrate-a-public-holiday`](https://cviceni-test.historylab.now.sh/dev/how-did-they-celebrate-a-public-holiday/)
- Added [`proc-vytvareli-mapu`](https://cviceni-test.historylab.now.sh/dev/proc-vytvareli-mapu/)
- Added [`what-happened-at-croydon-airport`](https://cviceni-test.historylab.now.sh/dev/what-happened-at-croydon-airport/)
- Added [`why-were-they-displaced`](https://cviceni-test.historylab.now.sh/dev/why-were-they-displaced/)
- Optimised [`videt-a-slyset-srpnovou-okupaci`](https://cviceni-test.historylab.now.sh/dev/videt-a-slyset-srpnovou-okupaci/)
- Optimised [`vztycovani-vlajky`](https://cviceni-test.historylab.now.sh/dev/vztycovani-vlajky/)

### Development
- Do not use CSS modifiers on slides but on individual modules instead with `layout` property or other
    - Removed `.slide-layout-column`, `.slide-direction-reverse`
- Added `npm run prerelease` script with tests
    - `number equality`, `id duplicates`
- Run pipeline on tag only
- Ignored `CIRCULAR_DEPENDENCY` warning from Rollup

### General
- **Add language & translation support**
    - new `src/translations/index.js` translation object
    - WIP: translations of content in CSS
- Fixed typesetting: (even mutliple) single character are typesetted properly

### Tools
- **SVG supports duplicates from other SVGs**
    - new property `svg.duplikovat`
- **SVG supports additional gallery**
    - new property `svg.galerie`
- **Sortable supports customized feedback**
    - new property `razeni.zpetnaVazba`
    - new property `razeni.objekty[objekt].spravnaOdpoved`
- Add label to audio player
    - new property `media.soboury[soubor].label`
- Improved additional gallery with `position: sticky`
- Fixed analytical table: forbid to drop tag on `<input>` now


## 5.3.1 — 13.6.2019
### Tools
- Fixed: SVG text wasn't removed when empty

## 5.3.0 — 11.6.2019
### Development
- **Added custom `__dispatchEvent($element, type, options, payload)` function**
- Fix pipelines to run on `master` branch only
- Updated `doc/release-checklist.md`

### Analytics
- Improved analytics for navigation (track index of active slide for each interaction)

## 5.2.3 — 11.6.2019
### Analytics
- Fixed interaction analytics (removed old code)

## 5.1.0 — 5.2.1 — 10.6.2019 (fucked up deployments)
### Exercises
- Added [`co-nataceli`](https://cviceni-test.historylab.now.sh/dev/co-nataceli/)
- Added [`co-se-deje-na-fotografii`](https://cviceni-test.historylab.now.sh/dev/co-se-deje-na-fotografii/)
- Added [`deset-let-po-revoluci-dekujeme-odejdete`](https://cviceni-test.historylab.now.sh/dev/deset-let-po-revoluci-dekujeme-odejdete/)
- Updated [`proc-jezdit-bezpecne`](https://cviceni-test.historylab.now.sh/dev/proc-jezdit-bezpecne/)
- Updated [`proc-sedeli-ve-skole`](https://cviceni-test.historylab.now.sh/dev/proc-sedeli-ve-skole/)
- Updated [`videt-a-slyset-srpnovou-okupaci`](https://cviceni-test.historylab.now.sh/dev/videt-a-slyset-srpnovou-okupaci/)

### Analytics
- Improved interaction analytics for `keydown` and `popstate`
    - two new properties: `type`, `name`


## 5.0.0 — 7.6.2019
### Development
- **Added automatic generation of slides from JSON data**
    - use `index.pug` for custom content/functionality only
- **Added automatic typesetting on build time** (use `!{typeset(stringValue)}`)
- **Unify audio & video modul into media modul**
    - `modul-audio.pug`, `modul-video.pug` are submodules of `modul-media.pug`
    - `media` property in JSON
- Improved text editor to generate `text-option` items on build instead of client
- Add slide.change customEvent and use it to stop all players & move audio and video to media folder and anstract some code
- Unify table modules into one master `modul-tabulka.pug`
- Renamed `obecnaTabulka` to `tabulkaObecna`
- Removed `faksimile` property from JSON
- Fixed notation of `layout: "velka-galerie"` parameter (JSON, PUG files)
- Removed `pretty` option from pug config - it is deprecated - no need anyway
- Fixed scaffold templates
- Removed custom index page
- Added script to test of uniqness of `cviceni.id`
- Added `doc/release-checklist.md`
- Added `doc/instrukce-k-deployment-prototypu.md`
- Updated `doc/jak-vytvorit-nove-cviceni.md`

### Exercises
- Added [`co-oslavovali`](https://cviceni-test.historylab.now.sh/dev/co-oslavovali/)
- Added [`co-delaji-zeny-na-fotografii`](https://cviceni-test.historylab.now.sh/dev/co-delaji-zeny-na-fotografii/)
- Added [`jak-si-pripominame-listopad-na-albertove`](https://cviceni-test.historylab.now.sh/dev/jak-si-pripominame-listopad-na-albertove/)
- Added [`proc-sedeli-ve-skole`](https://cviceni-test.historylab.now.sh/dev/proc-sedeli-ve-skole/)
- Added [`proc-jezdit-bezpecne`](https://cviceni-test.historylab.now.sh/dev/proc-jezdit-bezpecne/)
- Added [`promeny-obce-abertamy`](https://cviceni-test.historylab.now.sh/dev/promeny-obce-abertamy/)
- Added [`vanocni-zmena`](https://cviceni-test.historylab.now.sh/dev/vanocni-zmena/)
- Added [`videt-a-slyset-srpnovou-okupaci`](https://cviceni-test.historylab.now.sh/dev/videt-a-slyset-srpnovou-okupaci/)
- Updated [`napalm-girl`](https://cviceni-test.historylab.now.sh/dev/napalm-girl/)
- Updated [`proc-byli-uneseni`](https://cviceni-test.historylab.now.sh/dev/proc-byli-uneseni/)
- Updated [`promeny-mesta-zlin`](https://cviceni-test.historylab.now.sh/dev/promeny-mesta-zlin/)
- Updated [`statni-svatek`](https://cviceni-test.historylab.now.sh/dev/statni-svatek/)
- Updated [`to-byla-slava`](https://cviceni-test.historylab.now.sh/dev/to-byla-slava/)

### General
- Automated typesetting
- Improved mobile & tablet experience
    - ignore half-slide
    - fixed spacing of textareas

### Tools
- **Added video player**
- **Gallery can show/play audio and video**
- **Sorting can show/play video** (audio already supported)

### Analytics
- Track type of element which user used for navigation to `analytics.activity.steps.interaction`
    - `history-back`, `history-forward`, `feedback`, `navigation`

## 4.1.1 — 14.3.2019
### Exercises & development
* Fix not displaying image in SVG due async function 

## 4.1.0 — 8.3.2019
### Development
* Added `doc/jak-vytvorit-nove-cviceni.md` & update `readme.md`
* Added automated creation of slide IDs (no more manual work in JSON!)
* Added automated image dimensions for SVG (no more manual work in JSON!)
* Updated scaffold templates

### Tools
* Added simple analytics for SVG (number of `points`, `paths`, `comments`)

### General
* Added support for keyboard control to navigation
    * `right`, `space`, `enter` to go next
    * `left`, `shift-space`, `shift-enter` to go back
* Update feedback button

### Exercises
* Added new exercise `vztycovani-vlajky`
* Updated `napalm-girl`


## 4.0.3 — 14.2.2019
### Exercises
* Fixed help in `napalm-girl`
* Removed unused meta data in all exercises


## 4.0.2 — 14.2.2019
### Development
* Removed & ignored `package-lock.json` because of pipelines


## 4.0.1 — 14.2.2019
### Exercises
* Fixed `id` of `editor`


## 4.0.0 — 14.2.2019
### Development
* Add ability to build only one specific exercise with `--cviceni` argument (eg.: `gulp --cviceni napalm-girl`)
* Use `rollup-plugin-terser` instead of `rollup-plugin-uglify`
* Add `katalog` metadata for each exercise

### Tools
* Fixed magnifying glass in fullscreen
* Fixed SVG module imports

### Exercises
* Updated `korejske-deti-ve-skole`, `napalm-girl`, `promeny-obce-horni-vysoke`
* Add normalized audio files for `proc-byli-uneseni`


## 3.2.0 — 26.11.2018
### Development
* Add bitbucket pipelines to `master`

### Tools
* Fixed magnifying glass in other layers of image switcher (fake-map)
* Fixed magnifying glass in fullscreen
* Fixed to play audio again if ended in the simple player

### Exercises
* Fixed `maxLength` for text inputs in `jak-psat-o-uprchlicich`
* Updated & optimized images for `co-ministr-vzkazuje-ucitelum` and`proc-byli-uneseni`
* Fixed typo in `mnichov-1938`


## 3.1.0 — 9.11.2018
### General
* Přidán CHANGELOG.md
### Exercises
* Opraveny názvy souborů s doporučeným postupem pro cvičení _Netradiční mezníky československých dějin?_ a _Proč vystavovali obraz T. G. Masaryka?_
### Tools
* Lupa nyní funguje i na obrázku zvětšeném přes celou obrazovku
* Nově vzniklé textové bubliny mění orientaci v závislosti na jejich pozici vůči okrajům obrázku, aby se nestalo, že se textové pole umístí zcela mimo obraz.
* Přidána tečka k textové bublině kvůli vizuálnímu zdůraznění místa, ke kterému byla textová bublina umístěna.
